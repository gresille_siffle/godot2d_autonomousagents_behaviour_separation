extends Node2D

"""
The Simulation
--------------

This node is responsible of running the boids simulation and handling the user interface signals.

Its composed of a `Grid` node, a two-dimensional bin-lattice spacial subdivision that saves all boids coordinates,
allowing them to interact only with their close neighbours, and thus preventing a lot of useless and time consuming
operations (see « Locality Queries » in http://www.red3d.com/cwr/papers/2000/pip.pdf by Craig W. Reynolds).

A `Boids` node, containing all boids. And finally a user interface.
"""

const BOID_SCENE : PackedScene = preload('res://scenes/boid/Boid.tscn')

export (int) var initial_boids_nbr = 64
export (int) var maximum_boids_nbr = 256

onready var grid : Grid_ = $Grid
onready var boids : Node2D = $Boids
onready var interface : Control = $InterfaceLayer/Interface

var boid_max_force : int = 16
var boid_max_speed : int = 512
var boid_separation_distance : int = 32

var _grid_must_be_rebuilt : bool = false

func _ready() -> void:
# warning-ignore:return_value_discarded
    (get_tree().get_root() as Viewport).connect('size_changed', self, '_on_Viewport_size_changed')

    interface.initiaize(boid_max_force, boid_max_speed, initial_boids_nbr, maximum_boids_nbr, boid_separation_distance)
# warning-ignore:return_value_discarded
    interface.connect('max_force_changed', self, '_on_Interface_max_force_changed')
# warning-ignore:return_value_discarded
    interface.connect('max_speed_changed', self, '_on_Interface_max_speed_changed')
# warning-ignore:return_value_discarded
    interface.connect('boids_number_changed', self, '_on_Interface_boids_number_changed')
# warning-ignore:return_value_discarded
    interface.connect('separation_distance_changed', self, '_on_Interface_separation_distance_changed')

# warning-ignore:unused_variable
    for i in range(initial_boids_nbr):
        var boid : Boid = _new_boid()
        grid.add(boid)
        boids.add_child(boid)

# warning-ignore:unused_argument
func _process(delta : float) -> void:
    """Make the boids move and update the grid."""
    # 1. clear the grid, or rebuild it if the screen size has changed
    if _grid_must_be_rebuilt:
        _grid_must_be_rebuilt = false
        grid.rebuild(get_viewport_rect().size)
    else:
        grid.clear()

    # 2. fill the grid with the new boids according to their position
    grid.add_many(boids.get_children())

    # 3. update the boid neighborhood one by one
    for boid in boids.get_children():
        boid.apply_force(
            boid.separation(
                grid.neighborhood(int(boid.position.x / grid.resolution), int(boid.position.y / grid.resolution))
            )
        )
        boid.move(delta)

func _new_boid() -> Boid:
    var boid = BOID_SCENE.instance()
    randomize()
    var x : float = rand_range(-1, 1)
    randomize()
    var y : float = rand_range(-1, 1)
    boid.position = Vector2(self.get_viewport_rect().size.x / 2, self.get_viewport_rect().size.y / 2) + Vector2(x, y)
    boid.max_force = boid_max_force
    boid.max_speed = boid_max_speed
    boid.separation_distance = boid_separation_distance
    return boid

func set_boids_max_force(max_force : int) -> void:
    boid_max_force = max_force
    for boid in boids.get_children():
        (boid as Boid).max_force = max_force

func set_boids_max_speed(max_speed : int) -> void:
    boid_max_speed = max_speed
    for boid in boids.get_children():
        (boid as Boid).max_speed = max_speed

func set_boids_separation_distance(separation_distance : int) -> void:
    boid_separation_distance = separation_distance
    for boid in boids.get_children():
        (boid as Boid).separation_distance = separation_distance

func set_population_to(number : int) -> void:
    var current_population = boids.get_child_count()

    # add more boids
    if current_population < number:
# warning-ignore:unused_variable
        for i in range(number - current_population):
            boids.add_child(_new_boid())

    # reduce the population
    elif current_population > number:
        var i : int = 0
        for boid in boids.get_children():
            i += 1
            if i > number:
                boid.queue_free()
    else:
        pass

func _on_Viewport_size_changed() -> void:
    _grid_must_be_rebuilt = true

func _on_Interface_max_force_changed(value : float) -> void:
# warning-ignore:narrowing_conversion
    set_boids_max_force(value)

func _on_Interface_max_speed_changed(value : float) -> void:
# warning-ignore:narrowing_conversion
    set_boids_max_speed(value)

func _on_Interface_boids_number_changed(value : float) -> void:
# warning-ignore:narrowing_conversion
    set_population_to(value)

func _on_Interface_separation_distance_changed(value : float) -> void:
# warning-ignore:narrowing_conversion
    set_boids_separation_distance(value)
