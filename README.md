## Godot 2D - Autonomous Agents Behaviour - Separation

> An implementation of the steering behaviour "Separation" described by [Craig W. Reynolds] in its document [Steering Behaviors For Autonomous Characters].

[Craig W. Reynolds]: <https://www.red3d.com/cwr/>
[Steering Behaviors For Autonomous Characters]: <https://www.red3d.com/cwr/steer/gdc99/>

This program displays the separation behaviour of several autonomous agents, represented by bird-like objects called boids.

This group behaviour, also know as avoidance, is the result of local and individual interactions between several boids.
Each of them keeping a minimum distance with the others to avoid crowding.

The program run on the [Godot] [1] 3.1.1 game engine, and is made with the [GDScript] language.

![Simulation Screenshot ](/meta/autonomousagents_behaviour_separation.png)

## Table of Content

- [Install](#Install)
- [Usage](#Usage)
- [License](#License)

## Install

### Dependencies

Download [Godot] [2] 3.1.1 game engine. A self-contained executable of few megabytes, no installation required.

### Open the project

Download the source code and import the `project.godot` file from Godot 3.1.1.

## Usage

... todo ...

## License

[MIT](LICENSE.txt) (c) Gresille&Siffle

[1]: <https://godotengine.org/> "Godot game engine site"
[2]: <https://godotengine.org/download> "Godot game engine download page"
[GDScript]: <http://docs.godotengine.org/en/3.0/getting_started/scripting/gdscript/gdscript_basics.html>
